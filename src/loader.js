const fs = require("fs");
const path = require("path");
const _ = require('lodash');
const jsYaml = require("js-yaml");

module.exports = function() {
  try {
    const teamFilePath = path.join(__dirname, "team.yml");
    const file = fs.readFileSync(teamFilePath, "utf8");
    results = jsYaml.safeLoad(file, { json: true });
    results = results.filter(
      member => member.departments.indexOf("Vacancy") === -1
    );

    results.map(member => {
      if (member.hasOwnProperty("projects")) {
        // Make projects key a little more useful
        for (let project in member.projects) {
          let obj = {};
          let type, labels;

          member.projects[project].split(" ").forEach((tag, idx, arr) => {
            if (["reviewer", "maintainer"].indexOf(tag) > -1) {
              type = arr.splice(idx, 1)[0];
              labels = arr;
            }
          });

          member.projects[project] = {
            type,
            labels
          };
        }
      }

      member.cleanedReportsToSlug = cleanSlug(member.reports_to);
      member.cleanedRoleSlug = cleanSlug(member.role_slug);
      member.picture = createPictureUrl(member.picture);
      member.role = member.role_slug.replace(/\b[a-z]/g, letter => letter.toUpperCase()).replace(/-/g, ' ');

      return member;
    });

    return results;
  } catch (e) {
    console.error(e);
  }
};

function cleanSlug(slug) {
  if (!slug) {
    return;
  }

  return slug
    .split("-")
    .filter(tag => {
      return (
        [
          "director",
          "of",
          "engineering",
          "fellow",
          "interim",
          "manager",
          "lead"
        ].indexOf(tag) === -1
      );
    }, "")
    .join("-");
}

function createPictureUrl(filename) {
  let name = filename.split('.')[0];

  return `https://about.gitlab.com/images/team/${name}-crop.jpg`;
}
